package com.example.kkrasilnikov.l13_custoviewexample;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomExampleView extends RelativeLayout {
    private TextView m_tw = null;
    private ImageView m_iw = null;

    public CustomExampleView(Context context) {
        super(context);
        initLayout(context);
    }

    public CustomExampleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initLayout(context);
    }

    public CustomExampleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initLayout(context);
    }

    private void initLayout(Context context){
        LayoutInflater.from(context).inflate(R.layout.view_custom_example,this,true);
        m_tw = (TextView) findViewById(R.id.textView);
        m_iw = (ImageView) findViewById(R.id.imageView);
    }


    /*Метод который вызывается в момент отрисовки объекта*/
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        //Получаем реальную ширину объекта (с учетом всех отступов и других объектов)
        int nRealWidth = MeasureSpec.getSize(widthMeasureSpec);

        //Получаем Bitmap изображения в ImageView
        Bitmap curBitmap = ((BitmapDrawable)m_iw.getDrawable()).getBitmap();
        //Проверяем условие - что высота меньше длины
        if(curBitmap.getHeight()<curBitmap.getWidth()){
            //Если условие удовлетворяется, переварачиваем битмапу на 270 (можно на 90)
            //Для этого создаем класс отвечающий за преобразованиея
            Matrix matrix = new Matrix();
            //И указываем что используем переворот на 270 градусов
            matrix.postRotate(270);
            //Создаем новую битмапу, на основе старой + используя преобразование
            Bitmap rotatedBitmap = Bitmap.createBitmap(curBitmap, 0, 0,  curBitmap.getWidth(), curBitmap.getHeight(), matrix, true);

            //Устанавливаем новую битмапу
            m_iw.setImageBitmap(rotatedBitmap);
        }

        //Перерисовываем вью после изменений
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        //Высчитываем левый отступ, для центрирования изображения (из реальной ширины, отнимаем ширину картинки и делим все по-полам)
        int nMarginLeft = (nRealWidth - m_iw.getMeasuredWidth())/2;

        //Берем LayoutParams у ImageView
        LayoutParams ilp = (LayoutParams) m_iw.getLayoutParams();
        //И выставляем левый отступ
        ilp.setMargins(nMarginLeft,0,0,0);

        //Для того что бы опустить текст ниже картинки, берем его LayoutParams
        LayoutParams lp = (LayoutParams) m_tw.getLayoutParams();
        //И устанавливаем отступ с верху который равный высоте ImageView + константа (что бы был отступ, а не текст в притык)
        lp.setMargins(0,m_iw.getMeasuredHeight()+5,0,0);

        //Если текст занимает больше одной строки, необходимо изменить размер шрифта
        if (m_tw.getLineCount()>1){
            //Для того что бы подобрать размер шрифта организуем цикл с пост условием
            do{
                //Уменьшаем размер шрифта на 1 пиксель
                m_tw.setTextSize(TypedValue.COMPLEX_UNIT_PX,m_tw.getTextSize()-1);
                //Перерисовуем наш объект
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                //Условием продолжения работы цикла есть тот факт, что наш TextView занимает больше 1 строки
            }while(m_tw.getLineCount()>1);
        }

    }
}