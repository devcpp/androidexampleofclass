package com.example.jul.l14_databaseandsharedprefexample.Model;


import android.content.ContentValues;
import android.database.Cursor;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;

public class RecordInNotepad {

    private long m_nId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private String m_strPhone = "";

    public RecordInNotepad(Cursor cursor){
        setId(cursor.getInt(0));
        setName(cursor.getString(1));
        setSName(cursor.getString(2));
        setPhone(cursor.getString(3));
    }

    public RecordInNotepad(long nId, Cursor cursor){
        setId(nId);
        setName(cursor.getString(0));
        setSName(cursor.getString(1));
        setPhone(cursor.getString(2));
    }

    public RecordInNotepad(String strName, String strSName, String strPhone){
        setName(strName);
        setSName(strSName);
        setPhone(strPhone);
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public long getId() {
        return m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_NAME,getName());
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_SNAME,getSName());
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_PHONE,getPhone());
        return contentValues;
    }
}
