package com.example.jul.l14_databaseandsharedprefexample.Model.DataEngine;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.DBWrappers.RecordDBWrapper;

import java.util.ArrayList;

public class RecordEngine extends BaseEngine {

    public RecordEngine(Context context) {
        super(context);
    }

    public ArrayList<RecordInNotepad> getAll(){
        return getListBySearchString("");
    }

    public ArrayList<RecordInNotepad> getListBySearchString(String strSearch){
        Log.d("devcpp", "getListBySearchString -> strSearch -> " + strSearch);
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        return wrapper.getListBySearchString(strSearch);
    }

    public RecordInNotepad getItemById(long nId){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        return wrapper.getItemById(nId);
    }

    public void addItem(RecordInNotepad item){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        wrapper.insertItem(item);
    }

    public void updateItem(RecordInNotepad item){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        wrapper.updateItem(item);
    }

    public void removeItem(RecordInNotepad item){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        wrapper.removeItem(item);
    }

    public void removeAll(){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        wrapper.removeAll();
    }
}
