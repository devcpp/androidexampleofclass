package com.example.jul.l14_databaseandsharedprefexample.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.R;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class NotepadAdapter extends BaseAdapter {

    private ArrayList<RecordInNotepad> m_arrData = null;

    public NotepadAdapter(ArrayList<RecordInNotepad> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((RecordInNotepad)getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        /*Log.d("devcpp","position -> " + position);
        Log.d("devcpp","convertView -> " + convertView);*/


        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.custom_view_item, parent, false);
        }

        TextView tvName = (TextView) convertView.findViewById(R.id.tvNameItem);
        TextView tvSName = (TextView) convertView.findViewById(R.id.tvSNameItem);
        TextView tvPhone = (TextView) convertView.findViewById(R.id.tvPhoneItem);

        RecordInNotepad item = (RecordInNotepad) getItem(position);
        /*Log.d("devcpp","item.getName() -> " + item.getName());
        Log.d("devcpp","<--------------------------> ");*/
        tvName.setText(item.getName());
        tvSName.setText(item.getSName());
        tvPhone.setText(item.getPhone());

        return convertView;
    }
}
