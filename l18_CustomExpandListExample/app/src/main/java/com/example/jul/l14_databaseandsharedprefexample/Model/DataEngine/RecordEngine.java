package com.example.jul.l14_databaseandsharedprefexample.Model.DataEngine;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.example.jul.l14_databaseandsharedprefexample.Model.PhoneNumber;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.DBWrappers.RecordDBWrapper;

import java.util.ArrayList;

public class RecordEngine extends BaseEngine {

    public RecordEngine(Context context) {
        super(context);
    }

    public ArrayList<RecordInNotepad> getAll(){
        return getListBySearchString("");
    }

    public ArrayList<RecordInNotepad> getListBySearchString(String strSearch){
        Log.d("devcpp", "getListBySearchString -> strSearch -> " + strSearch);
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        ArrayList<RecordInNotepad> arrData = wrapper.getListBySearchString(strSearch);
        for(RecordInNotepad record:arrData){
            long nIdInfo = record.getId();
            record.setPhone(phoneEngine.getPhonesByInfoId(nIdInfo));
        }
        return arrData;
    }

    public RecordInNotepad getItemById(long nId){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        RecordInNotepad record = wrapper.getItemById(nId);
        if(record!=null){
            long nIdInfo = record.getId();
            PhoneEngine phoneEngine = new PhoneEngine(getContext());
            record.setPhone(phoneEngine.getPhonesByInfoId(nIdInfo));
        }
        return record;
    }

    public void addItem(RecordInNotepad item){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        long nIdInfo = wrapper.insertItem(item);
        ArrayList<PhoneNumber> arrPhones = item.getPhone();
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        for (PhoneNumber phone:arrPhones){
            phone.setIdInfo(nIdInfo);
            phoneEngine.addItem(phone);
        }
    }

    public void updateItem(RecordInNotepad item){
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        wrapper.updateItem(item);
        long nIdInfo = item.getId();
        ArrayList<PhoneNumber> arrPhones = item.getPhone();
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        for (PhoneNumber phone:arrPhones){
            phone.setIdInfo(nIdInfo);
            if (phone.getId()>0){
                phoneEngine.updateItem(phone);
            } else {
                phoneEngine.addItem(phone);
            }
        }
    }

    public void removeItem(RecordInNotepad item){
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        ArrayList<PhoneNumber> arrPhones = item.getPhone();
        for (PhoneNumber phone:arrPhones){
            phoneEngine.removeItem(phone);
        }
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        wrapper.removeItem(item);


    }

    public void removeAll(){
        PhoneEngine phoneEngine = new PhoneEngine(getContext());
        phoneEngine.removeAll();
        RecordDBWrapper wrapper = new RecordDBWrapper(getContext());
        wrapper.removeAll();
    }
}
