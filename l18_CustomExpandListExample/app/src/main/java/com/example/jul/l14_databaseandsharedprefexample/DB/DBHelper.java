package com.example.jul.l14_databaseandsharedprefexample.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    //Имена таблиц в БД
    public static final String TABLE_NAME_PERSONAL_INFO = "PERSONAL_INFO";
    public static final String TABLE_NAME_PHONES = "PHONES";

    //Имена полей в БД
    public static final String PERSONAL_INFO_FIELD_ID = "_ID";
    public static final String PERSONAL_INFO_FIELD_NAME = "NAME";
    public static final String PERSONAL_INFO_FIELD_SNAME = "SNAME";

    public static final String PHONES_FIELD_ID = "_ID";
    public static final String PHONES_FIELD_ID_INFO = "ID_INFO";
    public static final String PHONES_FIELD_PHONE = "PHONE";


    //Конструктор
    public DBHelper(Context context) {
        //В нём вызывается конструктор базового класса по созданию БД
        super(context, "l14_db", null, 1);
    }

    //Функция вызывается если база данных отсутсвует
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME_PERSONAL_INFO + " ("
                + PERSONAL_INFO_FIELD_ID + " integer primary key autoincrement,"
                + PERSONAL_INFO_FIELD_NAME + " text,"
                + PERSONAL_INFO_FIELD_SNAME + " text);");

        db.execSQL("create table " + TABLE_NAME_PHONES + " ("
                + PHONES_FIELD_ID + " integer primary key autoincrement,"
                + PHONES_FIELD_ID_INFO + " integer,"
                + PHONES_FIELD_PHONE + " text);");
    }

    //Метод вызывается если база данных существует, но её версия отличается от текущей
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
    }
}
