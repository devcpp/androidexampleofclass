package com.example.jul.l14_databaseandsharedprefexample.Model.DataEngine;

import android.content.Context;

public class BaseEngine {

    private Context m_context = null;

    public BaseEngine(Context context){
        m_context = context;
    }

    public Context getContext(){
        return m_context;
    }

}
