package com.example.jul.l14_databaseandsharedprefexample.Model;


import android.content.ContentValues;
import android.database.Cursor;

public abstract class BaseEntity {
    private long m_nId = -1;

    BaseEntity(Cursor cursor){
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public long getId() {
        return m_nId;
    }

    public abstract ContentValues getContentValues();
}
