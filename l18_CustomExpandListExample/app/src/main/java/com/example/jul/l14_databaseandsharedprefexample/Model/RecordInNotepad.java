package com.example.jul.l14_databaseandsharedprefexample.Model;


import android.content.ContentValues;
import android.database.Cursor;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;

import java.util.ArrayList;

public class RecordInNotepad extends BaseEntity {
    private String m_strName = "";
    private String m_strSName = "";
    private ArrayList<PhoneNumber> m_arrPhones = new ArrayList<>();

    public RecordInNotepad(Cursor cursor){
        super(cursor);
        setId(cursor.getInt(0));
        setName(cursor.getString(1));
        setSName(cursor.getString(2));
    }

    public RecordInNotepad(long nId, Cursor cursor){
        super(cursor);
        setId(nId);
        setName(cursor.getString(0));
        setSName(cursor.getString(1));
    }

    public RecordInNotepad(String strName, String strSName){
        super(null);
        setName(strName);
        setSName(strSName);
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public void setPhone(ArrayList<PhoneNumber> arrPhones) {
        this.m_arrPhones = arrPhones;
    }


    public String getName() {
        return m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public ArrayList<PhoneNumber> getPhone() {
        return m_arrPhones;
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_NAME,getName());
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_SNAME,getSName());
        return contentValues;
    }
}
