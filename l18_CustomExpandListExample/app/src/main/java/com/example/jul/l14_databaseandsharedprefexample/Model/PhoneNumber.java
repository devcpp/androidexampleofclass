package com.example.jul.l14_databaseandsharedprefexample.Model;

import android.content.ContentValues;
import android.database.Cursor;


import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;

public class PhoneNumber extends  BaseEntity{
    private long m_nIdInfo = -1;
    private String m_strPhone = "";

    public PhoneNumber(Cursor cursor) {
        super(cursor);
        setId(cursor.getLong(0));
        setIdInfo(cursor.getLong(1));
        setPhone(cursor.getString(2));
    }

    public PhoneNumber(long nIdInfo, String strPhone) {
        super(null);
        setId(-1);
        setIdInfo(nIdInfo);
        setPhone(strPhone);
    }

    public void setIdInfo(long nIdInfo) {
        this.m_nIdInfo = nIdInfo;
    }

    public void setPhone(String strPhone) {
        this.m_strPhone = strPhone;
    }

    public long getIdInfo() {
        return m_nIdInfo;
    }

    public String getPhone() {
        return m_strPhone;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.PHONES_FIELD_ID_INFO, getIdInfo());
        contentValues.put(DBHelper.PHONES_FIELD_PHONE, getPhone());

        return contentValues;
    }
}

