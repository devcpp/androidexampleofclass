package com.example.jul.l14_databaseandsharedprefexample.Model.DataEngine;

import android.content.Context;

import com.example.jul.l14_databaseandsharedprefexample.Model.PhoneNumber;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.DBWrappers.PhoneDBWrapper;
import com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.DBWrappers.RecordDBWrapper;

import java.util.ArrayList;

public class PhoneEngine extends BaseEngine {
    public PhoneEngine(Context context) {
        super(context);
    }

    public ArrayList<PhoneNumber> getPhonesByInfoId(long nIdInfo){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(getContext());
        return wrapper.getPhonesByInfoId(nIdInfo);
    }

    public void addItem(PhoneNumber item){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(getContext());
        wrapper.insertItem(item);
    }

    public void updateItem(PhoneNumber item){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(getContext());
        wrapper.updateItem(item);
    }

    public void removeItem(PhoneNumber item){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(getContext());
        wrapper.removeItem(item);
    }

    public void removeAll(){
        PhoneDBWrapper wrapper = new PhoneDBWrapper(getContext());
        wrapper.removeAll();
    }
}
