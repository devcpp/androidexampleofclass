package com.kkrasylnykov.l28_contactexample.Activites;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.kkrasylnykov.l28_contactexample.R;

public class ActionActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String KEY_NAME = "KEY_NAME";
    public static final String KEY_EMAIL = "KEY_EMAIL";
    public static final String KEY_PHONE = "KEY_PHONE";

    private String m_strName = "";
    private String m_strEmail = "";
    private String m_strPhone = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);

        Intent intent = getIntent();
        if(intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                m_strName = bundle.getString(KEY_NAME,"");
                m_strEmail = bundle.getString(KEY_EMAIL,"");
                m_strPhone = bundle.getString(KEY_PHONE,"");
            }
        }

        Button callButton = (Button) findViewById(R.id.CallButton);
        callButton.setOnClickListener(this);
        Button browseButton = (Button) findViewById(R.id.BrowseButton);
        browseButton.setOnClickListener(this);
        Button sendEmailButton = (Button) findViewById(R.id.SendEmailButton);
        sendEmailButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.BrowseButton:
                Intent browseIntent = new Intent();
                String strUrl = "http://google.com/#q="+m_strName;
                browseIntent.setAction(Intent.ACTION_VIEW);
                browseIntent.setData(Uri.parse(strUrl));
                startActivity(browseIntent);
                break;
            case R.id.CallButton:
                String uri = "tel:" + m_strPhone.trim() ;
                Intent intentCall = new Intent(Intent.ACTION_DIAL);
                intentCall.setData(Uri.parse(uri));
                startActivity(intentCall);
                break;
            case R.id.SendEmailButton:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/html");
                intent.putExtra(Intent.EXTRA_EMAIL , m_strEmail);
                intent.putExtra(Intent.EXTRA_SUBJECT, "Тема");
                intent.putExtra(Intent.EXTRA_TEXT, "Тело письма.");
                //Добавление файла
                /**
                 File root = Environment.getExternalStorageDirectory();
                 File file = new File(root, xmlFilename);
                 if (!file.exists() || !file.canRead()) {
                 Toast.makeText(this, "Attachment Error", Toast.LENGTH_SHORT).show();
                 finish();
                 return;
                 }
                 Uri uri = Uri.parse("file://" + file);
                 intent.putExtra(Intent.EXTRA_STREAM, uri);
                 */
                startActivity(Intent.createChooser(intent, "Send email..."));


                break;
        }

    }
}
