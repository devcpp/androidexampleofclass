package com.kkrasylnykov.l28_contactexample.Activites;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.kkrasylnykov.l28_contactexample.Adapter.InfoAdapter;
import com.kkrasylnykov.l28_contactexample.Model.UserInfo;
import com.kkrasylnykov.l28_contactexample.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    ArrayList<UserInfo> m_arrData = new ArrayList<>();

    InfoAdapter m_adaInfoAdapter = null;
    ListView m_searchListView = null;
    EditText m_searchEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_searchEditText = (EditText) findViewById(R.id.SearchEditText);
        m_searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String strSerch = editable.toString();
                m_arrData.clear();
                if (!strSerch.isEmpty()){
                    m_arrData.addAll(getInfoByEmail(strSerch));
                }
                m_adaInfoAdapter.notifyDataSetChanged();
            }
        });

        m_searchListView = (ListView) findViewById(R.id.InfoListView);

        m_adaInfoAdapter = new InfoAdapter(m_arrData);
        m_searchListView.setAdapter(m_adaInfoAdapter);
        m_searchListView.setOnItemClickListener(this);

    }

    public ArrayList<UserInfo> getInfoByEmail(String strSerch){
        ArrayList<UserInfo> arrData = new ArrayList<>();
        Cursor cursor = null;
        try{
            String strSearchEmail = "%" +strSerch + "%";
            String[] PROJECTION = new String[] { ContactsContract.RawContacts._ID,
                    ContactsContract.Contacts.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
                    ContactsContract.CommonDataKinds.Email.DATA,
                    ContactsContract.CommonDataKinds.Email.CONTACT_ID,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER};
            cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, ContactsContract.CommonDataKinds.Email.DATA + " LIKE ?", new String[]{strSearchEmail}, null);
            if(cursor != null && cursor.moveToFirst()) {
                do {
                    //Получаем ID пользователя через курсор.
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID));
                    String strName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String strEmail = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                    Log.d("devcpp", "strName -> " + strName);
                    if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                        if (pCur != null && pCur.moveToFirst()) {
                            do {
                                String contactNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                arrData.add(new UserInfo(strName, strEmail, contactNumber));
                            } while (pCur.moveToNext());
                            pCur.close();
                        }
                    }
                } while (cursor.moveToNext());
            }
        }finally {
            if (cursor!=null) cursor.close();
        }

        return arrData;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, ActionActivity.class);
        UserInfo info = m_arrData.get(i);
        intent.putExtra(ActionActivity.KEY_NAME, info.getName());
        intent.putExtra(ActionActivity.KEY_EMAIL, info.getEmail());
        intent.putExtra(ActionActivity.KEY_PHONE, info.getPhone());
        startActivity(intent);
    }
}
