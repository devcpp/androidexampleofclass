package com.kkrasylnykov.l28_contactexample.Adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.kkrasylnykov.l28_contactexample.Model.UserInfo;
import com.kkrasylnykov.l28_contactexample.R;

import java.util.ArrayList;

public class InfoAdapter extends BaseAdapter{

    private ArrayList<UserInfo> m_arrData = null;

    public InfoAdapter(ArrayList<UserInfo> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int i) {
        return m_arrData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return -1;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view==null){
            LayoutInflater li = (LayoutInflater)viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = li.inflate(R.layout.item_userinfo, viewGroup, false);
        }

        TextView tvName = (TextView) view.findViewById(R.id.NameItemUserInfo);
        TextView tvEmail = (TextView) view.findViewById(R.id.EmailItemUserInfo);
        TextView tvPhone = (TextView) view.findViewById(R.id.PhoneItemUserInfo);

        UserInfo item = (UserInfo) getItem(i);
        /*Log.d("devcpp","item.getName() -> " + item.getName());
        Log.d("devcpp","<--------------------------> ");*/
        tvName.setText(item.getName());
        tvEmail.setText(item.getEmail());
        tvPhone.setText(item.getPhone());
        return view;
    }
}
