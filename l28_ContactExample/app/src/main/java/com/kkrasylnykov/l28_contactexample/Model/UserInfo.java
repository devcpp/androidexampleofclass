package com.kkrasylnykov.l28_contactexample.Model;


public class UserInfo {
    String strName = "";
    String strEmail = "";
    String strPhone = "";

    public UserInfo(String strName, String strEmail, String strPhone) {
        this.strName = strName;
        this.strEmail = strEmail;
        this.strPhone = strPhone;
    }

    public String getName() {
        return strName;
    }

    public String getEmail() {
        return strEmail;
    }

    public String getPhone() {
        return strPhone;
    }
}
