package com.kkrasylnykov.l21_fileandpermitionexample;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class AdapterListView extends BaseAdapter {
    ArrayList<FileOnServer> m_arrData = null;
    private OnClickListener m_listener = null;

    public AdapterListView(ArrayList<FileOnServer> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }

    @Override
    public Object getItem(int position) {
        return m_arrData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.item_on_click_example, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.textview);
        tv.setText(((FileOnServer)getItem(position)).getLocalName());
        ImageView iv = (ImageView)convertView.findViewById(R.id.imageview);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m_listener!=null){
                    m_listener.onClick(position);
                }
            }
        });
        return convertView;
    }

    //инициализация интерфейса
    public void setClickListener(OnClickListener listener){
        m_listener = listener;
    }

    //Описание интерфейса
    public interface OnClickListener{
        public void onClick(int nPosition);
    }
}
