package com.kkrasylnykov.l21_fileandpermitionexample;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class ImageFragment extends Fragment {

    private static final String FILE_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/";

    private FileOnServer m_FileOnServer = null;
    private Bitmap m_Bitmap = null;
    private ImageView m_ImageView = null;

    private BroadcastReceiver m_DownloadBroadCaBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setImage();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        m_ImageView = (ImageView) view.findViewById(R.id.ImageView);
        return view;
    }

    public void setFilePath(FileOnServer fileOnServer){
        m_FileOnServer = fileOnServer;
    }

    @Override
    public void onResume() {
        if (m_Bitmap==null){
            String strFilePath = FILE_PATH + m_FileOnServer.getLocalName();
            File file = new File(strFilePath);
            if (file.exists()){
                setImage();
            } else {
                Toast.makeText(getActivity(), "Not File " + strFilePath, Toast.LENGTH_LONG).show();
                Intent loadFile = new Intent(getActivity(),DownloadService.class);
                loadFile.putExtra(DownloadService.KEY_FILE_ID, m_FileOnServer.getId());
                loadFile.putExtra(DownloadService.KEY_FILE_PATH, strFilePath);
                loadFile.putExtra(DownloadService.KEY_FILE_URL, m_FileOnServer.getServerUrl());
                getActivity().startService(loadFile);
                getActivity().registerReceiver(m_DownloadBroadCaBroadcastReceiver,new IntentFilter(DownloadService.ACTION_DOWNLOAD+m_FileOnServer.getId()));
            }
        } else {
            m_ImageView.setImageBitmap(m_Bitmap);
        }
        //;

        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        try{
            getActivity().unregisterReceiver(m_DownloadBroadCaBroadcastReceiver);
        } catch (IllegalArgumentException e){

        }

    }

    public void setImage(){
        String strFilePath = FILE_PATH + m_FileOnServer.getLocalName();

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int nDispleyWidth = size.x;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(strFilePath, bmOptions);

        int nFileWidth = bmOptions.outWidth;

        float fScale = (float)nDispleyWidth/(float)nFileWidth;
        int nScale = (nFileWidth/nDispleyWidth);


        bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize =nScale;
        m_Bitmap = BitmapFactory.decodeFile(strFilePath,bmOptions);

        m_ImageView.setImageBitmap(m_Bitmap);
    }
}
