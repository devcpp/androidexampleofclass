package com.kkrasylnykov.l21_fileandpermitionexample;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadService extends Service {

    public static final String ACTION_DOWNLOAD = "ACTION_DOWNLOAD";

    public static final String KEY_FILE_PATH = "KEY_FILE_PATH";
    public static final String KEY_FILE_URL = "KEY_FILE_URL";
    public static final String KEY_FILE_ID = "KEY_FILE_ID";

    static ArrayList<String> m_arrDowloaded = new ArrayList<>();
    static ExecutorService m_executorService = null;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String strFilePath = "";
        String strFileUrl = "";
        int nId = -1;

        if (m_executorService==null){
            m_executorService = Executors.newFixedThreadPool(1);
        }

        if (intent!=null){
            Bundle extras = intent.getExtras();
            if (extras!=null){
                strFilePath = extras.getString(KEY_FILE_PATH,"");
                strFileUrl = extras.getString(KEY_FILE_URL,"");
                nId = extras.getInt(KEY_FILE_ID, -1);
            }
        }

        if (strFilePath.isEmpty() || strFileUrl.isEmpty()){
            return 0;
        }

        Log.d("devcpp", "onStartCommand -> " + strFilePath);

        Log.d("devcpp", "onStartCommand -> m_arrDowloaded.indexOf(strFileUrl) -> " + m_arrDowloaded.indexOf(strFileUrl));

        //if (m_arrDowloaded.indexOf(strFileUrl)<0){
            m_arrDowloaded.add(strFilePath);
            FileOnServer file = new FileOnServer(nId,strFilePath, strFileUrl);

            Runnable loadFile = new LoadRunnable(file);
            new Thread(loadFile).start();
            //m_executorService.execute(loadFile);
        //}

        return super.onStartCommand(intent, flags, startId);
    }

    public class LoadRunnable implements Runnable{
        private FileOnServer m_file = null;

        public LoadRunnable(FileOnServer file){
            m_file = file;
        }

        @Override
        public void run() {

            URL url = null;
            try {
                Log.d("devcpp", "onStartCommand -> run -> 1 ->" + m_file.getServerUrl());
                url = new URL(m_file.getServerUrl());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(1000);
                conn.setConnectTimeout(1000);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                conn.connect();
                Log.d("devcpp", "onStartCommand -> run -> 2 ->" + m_file.getLocalName());
                int response = conn.getResponseCode();
                if (response!=200){
                    return;
                }
                Log.d("devcpp", "RunnableGET -> response -> " + m_file.getLocalName() + " -> " + response);
                int contentLength = conn.getContentLength();
                Log.d("devcpp", "RunnableGET -> contentLength -> " + m_file.getLocalName() + " -> " + contentLength);
                InputStream inputStream = conn.getInputStream();
                //Создаем файл на диске
                FileOutputStream outputStream = new FileOutputStream(m_file.getLocalName());

                int bytesRead = -1;
                byte[] buffer = new byte[4096];

                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }

                outputStream.close();
                inputStream.close();

                Intent intentBDownload = new Intent();
                intentBDownload.setAction(ACTION_DOWNLOAD+m_file.getId());
                sendBroadcast(intentBDownload);

                Log.d("devcpp", "RunnableGET -> response -> " + m_file.getLocalName() + " -> DOWNLOAD!!!");

            } catch (MalformedURLException e) {
                Log.e("devcpp", "MalformedURLException -> " + e.toString());
                e.printStackTrace();
            } catch (ProtocolException e) {
                Log.e("devcpp", "ProtocolException -> " + e.toString());
                e.printStackTrace();
            } catch (IOException e) {
                Log.e("devcpp", "IOException -> " + e.toString());
                e.printStackTrace();
            } catch (Exception e){
                Log.e("devcpp", "!!!Exception -> " + e.toString());
            }
        }
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
