package com.example.jul.l14_databaseandsharedprefexample.Activites;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jul.l14_databaseandsharedprefexample.Adapters.NotepadAdapter;
import com.example.jul.l14_databaseandsharedprefexample.Adapters.RecAdapter;
import com.example.jul.l14_databaseandsharedprefexample.Model.DataEngine.RecordEngine;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.R;
import com.example.jul.l14_databaseandsharedprefexample.ToolsAndConstans.AppSettings;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

public class MainListActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    RecyclerView m_listView = null;

    ArrayList<RecordInNotepad> m_arrRecords = new ArrayList<>();
    RecAdapter m_adapter = null;

    String m_strSerch = "";
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        AppSettings settings = AppSettings.getInstance(this);

        View btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.btnRemoveAll);
        btnRemoveAll.setOnClickListener(this);

        View btnAddRecord = findViewById(R.id.btnAddRecord);
        btnAddRecord.setOnClickListener(this);

        EditText editText = (EditText) findViewById(R.id.searchEditText);
        editText.addTextChangedListener(new TextWatcher() {
            String strOld = "";

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSerch = s.toString();
                viewInfo();
            }
        });


        m_listView = (RecyclerView) findViewById(R.id.list_view);
        m_adapter = new RecAdapter(m_arrRecords);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        m_listView.setLayoutManager(layoutManager);
        m_listView.setAdapter(m_adapter);

        //m_listView.setOnItemClickListener(this);

        if (settings.getIsFirstStartApp()) {
            settings.setIsFirstStartApp(false);

            Toast.makeText(this, "First Start", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "No First Start", Toast.LENGTH_LONG).show();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAdd:
                Intent intentCompouseActivity = new Intent(this, CompouseActivity.class);
                startActivity(intentCompouseActivity);
                break;
            case R.id.btnRemoveAll:
                /*RecordEngine engine = new RecordEngine(this);
                engine.removeAll();
                viewInfo();*/
                int nRemovePosition = m_arrRecords.size()-1;
                m_arrRecords.remove(nRemovePosition);
                m_adapter.notifyItemRemoved(nRemovePosition);
                m_adapter.notifyItemChanged(nRemovePosition);
                break;
            case R.id.btnAddRecord:
                RecordInNotepad recordInNotepad = new RecordInNotepad("Name1", "SName1", "224343");
                m_arrRecords.add(recordInNotepad);
                int nPosition = m_arrRecords.size()-1;
                m_adapter.notifyItemInserted(nPosition);
                m_adapter.notifyItemChanged(nPosition+1);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        viewInfo();
    }

    private void viewInfo() {
        m_arrRecords.clear();
        RecordEngine engine = new RecordEngine(this);
        if (m_strSerch.length() > 0) {
            Log.d("devcpp", "m_strSerch -> " + m_strSerch);
            m_arrRecords.addAll(engine.getListBySearchString(m_strSerch));
        } else {
            m_arrRecords.addAll(engine.getAll());
        }

        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("devcpp", "position -> " + position);
        Log.d("devcpp", "id -> " + id);
        Intent updateIntent = new Intent(this, CompouseActivity.class);
        updateIntent.putExtra(CompouseActivity.EXTRA_KEY_ID_REVORD, id);
        startActivity(updateIntent);
    }

}
