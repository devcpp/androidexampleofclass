package com.example.jul.l14_databaseandsharedprefexample.Activites;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;
import com.example.jul.l14_databaseandsharedprefexample.Model.DataEngine.RecordEngine;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.R;


public class CompouseActivity extends AppCompatActivity implements View.OnClickListener {

    public  static final String EXTRA_KEY_ID_REVORD = "EXTRA_KEY_ID_REVORD";

    private EditText m_etName = null;
    private EditText m_etSName = null;
    private EditText m_etPhone = null;

    private RecordInNotepad m_item = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compouse);

        long nId = -1;
        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                nId = bundle.getLong(EXTRA_KEY_ID_REVORD, -1);
            }
        }
        RecordEngine engine = new RecordEngine(this);
        m_item = engine.getItemById(nId);


        Button btnAdd = (Button) findViewById(R.id.btnAddCompouseActivity);
        btnAdd.setOnClickListener(this);

        Button btnRemove = (Button) findViewById(R.id.btnRemoveCompouseActivity);
        btnRemove.setOnClickListener(this);


        m_etName = (EditText) findViewById(R.id.etName);
        m_etSName = (EditText) findViewById(R.id.etSName);
        m_etPhone = (EditText) findViewById(R.id.etPhone);

        if(m_item!=null){
            btnAdd.setText("Update");
            btnRemove.setVisibility(View.VISIBLE);

            m_etName.setText(m_item.getName());
            m_etSName.setText(m_item.getSName());
            m_etPhone.setText(m_item.getPhone());

        }
    }


    @Override
    public void onClick(View v) {
        RecordEngine engine = new RecordEngine(this);
        switch (v.getId()){
            case R.id.btnRemoveCompouseActivity:
                {
                    engine.removeItem(m_item);
                    finish();
                }
                break;
            case R.id.btnAddCompouseActivity:
                String strName = m_etName.getText().toString();
                String strSName = m_etSName.getText().toString();
                String strPhone = m_etPhone.getText().toString();

                if(m_item==null){
                    m_item = new RecordInNotepad(strName, strSName, strPhone);
                } else {
                    m_item.setName(strName);
                    m_item.setSName(strSName);
                    m_item.setPhone(strPhone);
                }

                if(m_item.getId()==-1){
                    engine.addItem(m_item);
                } else {
                    engine.updateItem(m_item);
                }

                if(m_item.getId()!=-1){
                    finish();
                } else {
                    m_etName.setText("");
                    m_etSName.setText("");
                    m_etPhone.setText("");

                    m_item = null;

                    m_etName.requestFocus();
                }
                break;
        }
    }
}
