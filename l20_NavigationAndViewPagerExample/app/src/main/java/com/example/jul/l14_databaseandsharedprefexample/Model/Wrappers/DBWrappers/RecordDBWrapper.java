package com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.DBWrappers;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;

import java.util.ArrayList;

public class RecordDBWrapper extends BaseDBWrappers {
    public RecordDBWrapper(Context context) {
        super(context, DBHelper.TABLE_NAME_PERSONAL_INFO);
    }

    public ArrayList<RecordInNotepad> getListBySearchString(String strSearch) {

        String strRec = null;
        String strSerch = null;
        String[] arrSerch = null;
        if (!strSearch.isEmpty()) {
            strRec = DBHelper.PERSONAL_INFO_FIELD_NAME + " LIKE ? OR " + DBHelper.PERSONAL_INFO_FIELD_SNAME +
                    " LIKE ? OR " + DBHelper.PERSONAL_INFO_FIELD_PHONE + " LIKE ?";
            strSerch = "%" + strSearch + "%";
            arrSerch = new String[]{strSerch, strSerch, strSerch};
        }

        SQLiteDatabase db = getDbRead();

        Log.d("devcpp", "getListBySearchString -> strRec -> " + strRec);

        Cursor cursor = db.query(getTableName(), null, strRec, arrSerch, null, null, null);

        ArrayList<RecordInNotepad> arrRecords = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            //Выполняем цикл
            do {
                arrRecords.add(new RecordInNotepad(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();;

        return arrRecords;
    }

    public void removeItem(RecordInNotepad item){
        SQLiteDatabase db = getDbWrite();

        String strRec = DBHelper.PERSONAL_INFO_FIELD_ID + " == ?";

        String[] arrSerch = new String[]{Long.toString(item.getId())};

        db.delete(getTableName(),
                strRec,
                arrSerch);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getDbWrite();

        db.delete(getTableName(),
                null,
                null);
        db.close();
    }

    public RecordInNotepad getItemById(long nId){
        RecordInNotepad result = null;
        if (nId>=0){
            SQLiteDatabase db = getDbRead();

            Cursor cursor = db.query(getTableName(),new String[]{DBHelper.PERSONAL_INFO_FIELD_NAME, DBHelper.PERSONAL_INFO_FIELD_SNAME, DBHelper.PERSONAL_INFO_FIELD_PHONE},
                    DBHelper.PERSONAL_INFO_FIELD_ID + "=?",
                    new String[]{Long.toString(nId)},null,null,null);
            if(cursor!=null && cursor.moveToFirst()){
                result = new RecordInNotepad(nId, cursor);
            }
        }
        return result;
    }

    public void insertItem(RecordInNotepad item){
        SQLiteDatabase db = getDbWrite();
        db.insert(getTableName(), null, item.getContentValues());
    }

    public void updateItem(RecordInNotepad item){
        SQLiteDatabase db = getDbWrite();
        db.update(getTableName(), item.getContentValues(),
                DBHelper.PERSONAL_INFO_FIELD_ID + "=?",
                new String[]{Long.toString(item.getId())});
    }


}
