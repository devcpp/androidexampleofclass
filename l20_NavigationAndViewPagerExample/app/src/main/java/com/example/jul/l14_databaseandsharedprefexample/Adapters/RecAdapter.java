package com.example.jul.l14_databaseandsharedprefexample.Adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.R;

import java.util.ArrayList;

public class RecAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_RECORD = 0;
    public static final int TYPE_BOTTOM_ELEMENT = 1;


    private ArrayList<RecordInNotepad> m_arrData = null;

    public RecAdapter(ArrayList<RecordInNotepad> arrData){
        m_arrData = arrData;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder result = null;
        if(viewType==TYPE_RECORD){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_view_item, parent, false);
            result = new RecordInNotepadHolder(v);
        } else if (viewType==TYPE_BOTTOM_ELEMENT){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_bottom_item, parent, false);
            result = new BottomHolder(v);
        }
        return result;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position)==TYPE_RECORD){
            RecordInNotepadHolder recordInNotepadHolder = (RecordInNotepadHolder)holder;
            RecordInNotepad item = m_arrData.get(position);
            recordInNotepadHolder.tvName.setText(item.getName());
            recordInNotepadHolder.tvSName.setText(item.getSName());
            recordInNotepadHolder.tvPhone.setText(item.getPhone());

        } else if(getItemViewType(position)==TYPE_BOTTOM_ELEMENT){
            BottomHolder bottomHolder = (BottomHolder)holder;
            bottomHolder.tvInfo.setText("List contain " + m_arrData.size());

        }

    }

    @Override
    public int getItemViewType(int position) {
        int nType = TYPE_RECORD;
        if(position==m_arrData.size()){
            nType = TYPE_BOTTOM_ELEMENT;
        }

        return nType;
    }

    @Override
    public int getItemCount() {
        return m_arrData.size()+1;
    }

    public class RecordInNotepadHolder extends RecyclerView.ViewHolder{

        TextView tvName = null;
        TextView tvSName = null;
        TextView tvPhone = null;

        public RecordInNotepadHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.tvNameItem);
            tvSName = (TextView) itemView.findViewById(R.id.tvSNameItem);
            tvPhone = (TextView) itemView.findViewById(R.id.tvPhoneItem);
        }
    }

    public class BottomHolder extends RecyclerView.ViewHolder{

        TextView tvInfo = null;


        public BottomHolder(View itemView) {
            super(itemView);
            tvInfo = (TextView) itemView.findViewById(R.id.tvBottomInfo);
        }
    }
}
