package com.example.jul.l14_databaseandsharedprefexample.Model;


import android.database.Cursor;

public class RecordInNotepad {

    private int m_nId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private String m_strPhone = "";

    public RecordInNotepad(Cursor cursor){
        setId(cursor.getInt(0));
        setName(cursor.getString(1));
        setSName(cursor.getString(2));
        setPhone(cursor.getString(3));
    }

    public RecordInNotepad(int nId, Cursor cursor){
        setId(nId);
        setName(cursor.getString(0));
        setSName(cursor.getString(1));
        setPhone(cursor.getString(2));
    }

    public void setId(int m_nId) {
        this.m_nId = m_nId;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public int getId() {
        return m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public String getPhone() {
        return m_strPhone;
    }
}
