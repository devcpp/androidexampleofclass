package com.example.jul.l14_databaseandsharedprefexample.Activites;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jul.l14_databaseandsharedprefexample.CustomViews.CustomItemView;
import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;
import com.example.jul.l14_databaseandsharedprefexample.R;
import com.example.jul.l14_databaseandsharedprefexample.ToolsAndConstans.AppSettings;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout m_conteyer = null;

    String m_strSerch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = AppSettings.getInstance(this);

        View btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.btnRemoveAll);
        btnRemoveAll.setOnClickListener(this);

        EditText editText = (EditText) findViewById(R.id.searchEditText);
        editText.addTextChangedListener(new TextWatcher() {
            String strOld = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSerch = s.toString();
                onResume();
            }
        });

        m_conteyer = (LinearLayout) findViewById(R.id.conteynerLL);


        if (settings.getIsFirstStartApp()){
            settings.setIsFirstStartApp(false);

            Toast.makeText(this, "First Start", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "No First Start", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAdd:
                Intent intentCompouseActivity = new Intent(this,CompouseActivity.class);
                startActivity(intentCompouseActivity);
                break;
            case R.id.btnRemoveAll:
                DBHelper dbHelper = new DBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete(DBHelper.TABLE_NAME_PERSONAL_INFO,
                        null,
                        null);
                db.close();
                onResume();
                break;
        }

        if (v.getClass().getSimpleName().equals("CustomItemView")){
            CustomItemView view = (CustomItemView)v;
            int nId = view.getRecId();
            Intent updateIntent = new Intent(this, CompouseActivity.class);
            updateIntent.putExtra(CompouseActivity.EXTRA_KEY_ID_REVORD, nId);
            startActivity(updateIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Для отображения данных в БД мы используем Лайоут размещенный в файле activity_main.xml

        //Отчищаем содержимое
        m_conteyer.removeAllViews();

        //Создаем помощника работы с БД
        DBHelper dbHelper = new DBHelper(this);

        //Получаем БД на чтение.
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //Создаем запрос на получение всех данных из таблицы PERSONAL_INFO
        Cursor cursor = null;
        if(m_strSerch.length()>0){
            String strRec = DBHelper.PERSONAL_INFO_FIELD_NAME + " LIKE ? OR " + DBHelper.PERSONAL_INFO_FIELD_SNAME +
                    " LIKE ? OR " + DBHelper.PERSONAL_INFO_FIELD_PHONE + " LIKE ?";
            String strSerch = "%" + m_strSerch + "%";
            String[] arrSerch = new String[]{strSerch,strSerch,strSerch};

            cursor = db.query(DBHelper.TABLE_NAME_PERSONAL_INFO,null,strRec,arrSerch,null,null,null);
        } else {
            cursor = db.query(DBHelper.TABLE_NAME_PERSONAL_INFO,null,null,null,null,null,null);
        }


        //Данные для дебага
        /*Log.d("devcppDB", "cursor -> " + cursor);
        Log.d("devcppDB", "cursor.isFirst() -> " + cursor.moveToFirst());
        Log.d("devcppDB", "cursor.getCount() -> " + cursor.getCount());*/

        //Проверяем что бы курсор существовал и у него обязательно был первый элемент
        if(cursor!=null && cursor.moveToFirst()){
            //Выполняем цикл
            do{
                //Считываем данные по индексу
                int id = cursor.getInt(0);
                String strName = cursor.getString(1);
                String strSName = cursor.getString(2);
                String strPhone = cursor.getString(3);

                //Выводим данные в лог.
                Log.d("devcppDB", "id -> " + id);
                Log.d("devcppDB", "strName -> " + strName);
                Log.d("devcppDB", "strSName -> " + strSName);
                Log.d("devcppDB", "strPhone -> " + strPhone);


                //Создаем кастомное вью для отображения данных
                CustomItemView customItemView = new CustomItemView(this);
                //и заполняем его (кастом вью) данными, полученными из БД
                customItemView.setInfo(id,strName,strSName,strPhone);
                customItemView.setOnClickListener(this);


                //Добавляем в контейнер наше вью
                m_conteyer.addView(customItemView);
            }while(cursor.moveToNext());
        }
        //После работы закрываем курсор
        cursor.close();
        //И закрываем БД
        db.close();

    }
}
