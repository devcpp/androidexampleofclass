package com.example.jul.l14_databaseandsharedprefexample.Activites;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.jul.l14_databaseandsharedprefexample.Adapters.NotepadAdapter;
import com.example.jul.l14_databaseandsharedprefexample.CustomViews.CustomItemView;
import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.R;
import com.example.jul.l14_databaseandsharedprefexample.ToolsAndConstans.AppSettings;

import java.util.ArrayList;

public class MainListActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    ListView m_listView = null;

    ArrayList<RecordInNotepad> m_arrRecords = new ArrayList<>();
    NotepadAdapter m_adapter = null;

    String m_strSerch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        AppSettings settings = AppSettings.getInstance(this);

        View btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.btnRemoveAll);
        btnRemoveAll.setOnClickListener(this);

        EditText editText = (EditText) findViewById(R.id.searchEditText);
        editText.addTextChangedListener(new TextWatcher() {
            String strOld = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSerch = s.toString();
                viewInfo();
            }
        });


        m_listView = (ListView) findViewById(R.id.list_view);
        m_adapter = new NotepadAdapter(m_arrRecords);
        m_listView.setAdapter(m_adapter);
        m_listView.setOnItemClickListener(this);

        if (settings.getIsFirstStartApp()){
            settings.setIsFirstStartApp(false);

            Toast.makeText(this, "First Start", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "No First Start", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAdd:
                Intent intentCompouseActivity = new Intent(this,CompouseActivity.class);
                startActivity(intentCompouseActivity);
                break;
            case R.id.btnRemoveAll:
                DBHelper dbHelper = new DBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete(DBHelper.TABLE_NAME_PERSONAL_INFO,
                        null,
                        null);
                db.close();
                viewInfo();
                break;
        }

        if (v.getClass().getSimpleName().equals("CustomItemView")){
            CustomItemView view = (CustomItemView)v;
            int nId = view.getRecId();
            Intent updateIntent = new Intent(this, CompouseActivity.class);
            updateIntent.putExtra(CompouseActivity.EXTRA_KEY_ID_REVORD, nId);
            startActivity(updateIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        viewInfo();
    }

    private void viewInfo(){
        //Для отображения данных в БД мы используем Лайоут размещенный в файле activity_main.xml

        //Отчищаем содержимое
        //m_conteyer.removeAllViews();

        //Создаем помощника работы с БД
        DBHelper dbHelper = new DBHelper(this);

        //Получаем БД на чтение.
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        //Создаем запрос на получение всех данных из таблицы PERSONAL_INFO
        Cursor cursor = null;
        if(m_strSerch.length()>0){
            String strRec = DBHelper.PERSONAL_INFO_FIELD_NAME + " LIKE ? OR " + DBHelper.PERSONAL_INFO_FIELD_SNAME +
                    " LIKE ? OR " + DBHelper.PERSONAL_INFO_FIELD_PHONE + " LIKE ?";
            String strSerch = "%" + m_strSerch + "%";
            String[] arrSerch = new String[]{strSerch,strSerch,strSerch};

            cursor = db.query(DBHelper.TABLE_NAME_PERSONAL_INFO,null,strRec,arrSerch,null,null,null);
        } else {
            cursor = db.query(DBHelper.TABLE_NAME_PERSONAL_INFO,null,null,null,null,null,null);
        }

        m_arrRecords.clear();
        //Проверяем что бы курсор существовал и у него обязательно был первый элемент
        if(cursor!=null && cursor.moveToFirst()){
            //Выполняем цикл
            do{
                m_arrRecords.add(new RecordInNotepad(cursor));
            }while(cursor.moveToNext());
        }
        m_adapter.notifyDataSetChanged();
        //После работы закрываем курсор
        cursor.close();
        //И закрываем БД
        db.close();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("devcpp", "position -> " + position);
        Log.d("devcpp", "id -> " + id);
        Intent updateIntent = new Intent(this, CompouseActivity.class);
        updateIntent.putExtra(CompouseActivity.EXTRA_KEY_ID_REVORD, id);
        startActivity(updateIntent);
    }
}
