package com.example.jul.l14_databaseandsharedprefexample.Activites;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;
import com.example.jul.l14_databaseandsharedprefexample.R;


public class CompouseActivity extends AppCompatActivity implements View.OnClickListener {

    public  static final String EXTRA_KEY_ID_REVORD = "EXTRA_KEY_ID_REVORD";

    private EditText m_etName = null;
    private EditText m_etSName = null;
    private EditText m_etPhone = null;

    private long m_nId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compouse);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                m_nId = bundle.getLong(EXTRA_KEY_ID_REVORD, -1);
            }
        }


        Button btnAdd = (Button) findViewById(R.id.btnAddCompouseActivity);
        btnAdd.setOnClickListener(this);

        Button btnRemove = (Button) findViewById(R.id.btnRemoveCompouseActivity);
        btnRemove.setOnClickListener(this);


        m_etName = (EditText) findViewById(R.id.etName);
        m_etSName = (EditText) findViewById(R.id.etSName);
        m_etPhone = (EditText) findViewById(R.id.etPhone);

        if(m_nId!=-1){
            btnAdd.setText("Update");
            btnRemove.setVisibility(View.VISIBLE);

            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            Cursor cursor = db.query(DBHelper.TABLE_NAME_PERSONAL_INFO,new String[]{DBHelper.PERSONAL_INFO_FIELD_NAME, DBHelper.PERSONAL_INFO_FIELD_SNAME, DBHelper.PERSONAL_INFO_FIELD_PHONE},
                    DBHelper.PERSONAL_INFO_FIELD_ID + "=?",
                    new String[]{Long.toString(m_nId)},null,null,null);
            if(cursor!=null && cursor.moveToFirst()){
                String strName = cursor.getString(0);
                String strSName = cursor.getString(1);
                String strPhone = cursor.getString(2);

                m_etName.setText(strName);
                m_etSName.setText(strSName);
                m_etPhone.setText(strPhone);
            } else {
                Toast.makeText(this, "Sory!! Record not found!", Toast.LENGTH_LONG).show();
                finish();
            }
            cursor.close();
            db.close();

        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRemoveCompouseActivity:
                {
                    DBHelper dbHelper = new DBHelper(this);
                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    db.delete(DBHelper.TABLE_NAME_PERSONAL_INFO,
                            DBHelper.PERSONAL_INFO_FIELD_ID + "=?",
                            new String[]{Long.toString(m_nId)});
                    db.close();
                    finish();
                }
                break;
            case R.id.btnAddCompouseActivity:
                //Считываем данные для записи в БД
                String strName = m_etName.getText().toString();
                String strSName = m_etSName.getText().toString();
                String strPhone = m_etPhone.getText().toString();


                //Создаем помощника работы с БД
                DBHelper dbHelper = new DBHelper(this);
                //Получаем БД на запись.
                SQLiteDatabase db = dbHelper.getWritableDatabase();

                //Формируем данные для запроса (вносим имена полей и что в них будет записано)
                ContentValues contentValues = new ContentValues();
                contentValues.put(DBHelper.PERSONAL_INFO_FIELD_NAME,strName);
                contentValues.put(DBHelper.PERSONAL_INFO_FIELD_SNAME,strSName);
                contentValues.put(DBHelper.PERSONAL_INFO_FIELD_PHONE,strPhone);

                //Выполняем запрос на вставку новой записи в БД
                if(m_nId==-1){
                    db.insert(DBHelper.TABLE_NAME_PERSONAL_INFO, null, contentValues);
                } else {
                    db.update(DBHelper.TABLE_NAME_PERSONAL_INFO, contentValues,
                            DBHelper.PERSONAL_INFO_FIELD_ID + "=?",
                            new String[]{Long.toString(m_nId)});

                }


                //Обязательно закрыть БД
                db.close();
                if(m_nId!=-1){
                    finish();
                } else {
                    m_etName.setText("");
                    m_etSName.setText("");
                    m_etPhone.setText("");

                    m_etName.requestFocus();
                }
                break;
        }
    }
}
