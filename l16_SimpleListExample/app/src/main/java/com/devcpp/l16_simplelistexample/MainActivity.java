package com.devcpp.l16_simplelistexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    String[] citys = { "Харьков", "Киев", "Донецк", "Днепр", "Львов", "Луганск",
            "Симферополь", "Одесса", "Ровно", "Полтава", "Суммы", "Кривой Рог", " Житомир",
            "Запорожье", "Старый салтов" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView list_view = (ListView) findViewById(R.id.list_view);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, citys);

        list_view.setAdapter(adapter);
    }
}
