package com.kkrasylnykov.l29_externalappexample;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHORITY = "com.kkrasylnykov.l21_fileandpermitionexample.DataContentProvider";
    public static final String USER_INFO     = "user_info";

    public static final Uri USER_INFO_URI    = Uri.parse("content://" + AUTHORITY + "/" + USER_INFO);


    public static final String PERSONAL_INFO_FIELD_ID = "_ID";
    public static final String PERSONAL_INFO_FIELD_SERVER_ID = "SERVER_ID";
    public static final String PERSONAL_INFO_FIELD_NAME = "NAME";
    public static final String PERSONAL_INFO_FIELD_SNAME = "SNAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor cursor = getContentResolver().query(USER_INFO_URI,null,null,null,null);
        if (cursor!=null && cursor.moveToFirst()){
            do{
                //Данные считываем и выводим в лог
                long nLocal = cursor.getLong(0);
                long nServerLocal = cursor.getLong(1);
                String strName = cursor.getString(2);
                String strSName = cursor.getString(3);

                Log.d("devcppCP", "strName -> " + strName + " " + strSName);
                if (strName.indexOf("tost")>-1 || strSName.indexOf("tost")>-1){
                    Log.d("devcppCP", "if -> true");
                    //Меняем значения "test" на "tost"
                    ContentValues values = new ContentValues();
                    values.put(PERSONAL_INFO_FIELD_SERVER_ID, nServerLocal);
                    values.put(PERSONAL_INFO_FIELD_NAME, strName.replace("tost","tast"));
                    values.put(PERSONAL_INFO_FIELD_SNAME, strSName.replace("tost","tast"));
                    String strRequest = PERSONAL_INFO_FIELD_ID + "=?";
                    String[] arrArg = new String[]{Long.toString(nLocal)};
                    getContentResolver().update(USER_INFO_URI,values,strRequest,arrArg);
                }

            }while (cursor.moveToNext());
        }
        cursor.close();



    }
}
