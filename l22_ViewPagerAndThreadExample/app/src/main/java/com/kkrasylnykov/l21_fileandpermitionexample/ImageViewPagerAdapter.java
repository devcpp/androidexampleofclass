package com.kkrasylnykov.l21_fileandpermitionexample;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;

import java.io.File;
import java.util.ArrayList;

public class ImageViewPagerAdapter extends FragmentPagerAdapter {

    ArrayList<String> m_arrData = null;

    public ImageViewPagerAdapter(FragmentManager fm, ArrayList<String> arrData){
        super(fm);
        m_arrData = arrData;
    }

    @Override
    public Fragment getItem(int position) {
        ImageFragment imageFragment = new ImageFragment();
        imageFragment.setFilePath(m_arrData.get(position));
        return imageFragment;
    }

    @Override
    public int getCount() {
        return m_arrData.size();
    }
}
