package com.kkrasylnykov.l21_fileandpermitionexample;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class ImageFragment extends Fragment {

    private String m_strFilePath = null;
    private Bitmap m_Bitmap = null;
    private ImageView m_ImageView = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        m_ImageView = (ImageView) view.findViewById(R.id.ImageView);
        return view;
    }

    public void setFilePath(String strFilePath){
        m_strFilePath = strFilePath;
    }

    @Override
    public void onResume() {
        if (m_Bitmap==null){
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int nDispleyWidth = size.x;

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(m_strFilePath, bmOptions);

            int nFileWidth = bmOptions.outWidth;

            float fScale = (float)nDispleyWidth/(float)nFileWidth;
            int nScale = (nFileWidth/nDispleyWidth);


            bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize =nScale;
            m_Bitmap = BitmapFactory.decodeFile(m_strFilePath,bmOptions);
        }
        m_ImageView.setImageBitmap(m_Bitmap);

        super.onResume();
    }

    public void showInformation(){
        if(m_Bitmap==null){
            return;
        }

        Log.d("devcpp", "showInformation -> start");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("devcpp", "Thread -> start");
                int nWidth = m_Bitmap.getWidth();
                int nHeight = m_Bitmap.getHeight();

                int nCount = 0;
                for(int x=0; x<nWidth; x++){
                    for (int y=0; y<nHeight;y++){
                        int nColor = m_Bitmap.getPixel(x,y);
                        if(nColor==getActivity().getResources().getColor(android.R.color.white)){
                            nCount++;
                        }
                    }
                }
                final int nCountFinal = nCount;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("devcpp", "runOnUiThread -> start");
                        Toast.makeText(getActivity(),"Show information -> " + nCountFinal,Toast.LENGTH_LONG).show();
                        Log.d("devcpp", "runOnUiThread -> end");
                    }
                });

                Log.d("devcpp", "Thread -> center");

                nCount = 0;
                for(int x=0; x<nWidth; x++){
                    for (int y=0; y<nHeight;y++){
                        int nColor = m_Bitmap.getPixel(x,y);
                        if(nColor==getActivity().getResources().getColor(android.R.color.white)){
                            nCount++;
                        }
                    }
                }

                Log.d("devcpp", "Thread -> end");
            }
        });

        thread.start();
        Log.d("devcpp", "showInformation -> end");


    }
}
