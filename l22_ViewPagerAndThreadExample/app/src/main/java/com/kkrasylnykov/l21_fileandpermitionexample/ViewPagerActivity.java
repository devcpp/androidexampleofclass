package com.kkrasylnykov.l21_fileandpermitionexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;


public class ViewPagerActivity extends AppCompatActivity {

    public static final String KEY_FILES_PATH = "KEY_FILES_PATH";
    public static final String KEY_POSITION = "KEY_POSITION";

    ViewPager m_ViewPager = null;
    ImageViewPagerAdapter m_adapter = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        Intent intent = getIntent();
        Bundle bundleExtras = intent.getExtras();
        ArrayList arrData = null;
        int nPosition = 0;
        if(bundleExtras!=null){
            arrData = bundleExtras.getStringArrayList(KEY_FILES_PATH);
            nPosition = bundleExtras.getInt(KEY_POSITION,0);
        }

        m_ViewPager = (ViewPager) findViewById(R.id.viewPager);
        m_adapter = new ImageViewPagerAdapter(getSupportFragmentManager(),arrData);
        m_ViewPager.setAdapter(m_adapter);
        m_ViewPager.setCurrentItem(nPosition);
        m_ViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                getCurrentPage().showInformation();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public ImageFragment getCurrentPage(){
        ImageFragment returnPage = (ImageFragment) m_adapter.instantiateItem(m_ViewPager, m_ViewPager.getCurrentItem());
        return returnPage;
    }
}
