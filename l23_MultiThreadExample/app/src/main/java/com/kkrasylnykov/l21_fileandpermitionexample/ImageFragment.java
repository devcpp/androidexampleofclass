package com.kkrasylnykov.l21_fileandpermitionexample;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageFragment extends Fragment {

    private final static String KEY_TYPE = "KEY_TYPE";
    private final static String KEY_PROGRESS = "KEY_PROGRESS";
    private final static String KEY_COUNT = "KEY_COUNT";

    private final static int TYPE_IN_PROGRESS = 101;
    private final static int TYPE_COMPLITE = 102;

    private long m_nRedCount = 0;
    private long m_nGreanCount = 0;
    private long m_nBlueCount = 0;

    private String m_strFilePath = null;
    private Bitmap m_Bitmap = null;
    private ImageView m_ImageView = null;
    private TextView m_progressTextView = null;
    private CountThread m_thread = null;
    private Handler m_handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            if (bundle!=null){
                int nType = bundle.getInt(KEY_TYPE,TYPE_COMPLITE);
                if(nType==TYPE_IN_PROGRESS){
                    int nProgress = bundle.getInt(KEY_PROGRESS,0);
                    m_progressTextView.setText("Progtress: " + nProgress + "%");
                } else if(nType==TYPE_COMPLITE) {
                    long nCount = bundle.getLong(KEY_COUNT,0);
                    m_progressTextView.setText("This image have " + nCount + " white pixels.");
                }
            }
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        m_ImageView = (ImageView) view.findViewById(R.id.ImageView);
        m_progressTextView = (TextView) view.findViewById(R.id.progressTextView);
        return view;
    }

    public void setFilePath(String strFilePath){
        m_strFilePath = strFilePath;
    }

    @Override
    public void onResume() {
        if (m_Bitmap==null){
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int nDispleyWidth = size.x;

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(m_strFilePath, bmOptions);

            int nFileWidth = bmOptions.outWidth;

            float fScale = (float)nDispleyWidth/(float)nFileWidth;
            int nScale = (nFileWidth/nDispleyWidth);


            bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize =nScale-1;
            m_Bitmap = BitmapFactory.decodeFile(m_strFilePath,bmOptions);
        }
        m_ImageView.setImageBitmap(m_Bitmap);

        super.onResume();
    }

    public void onStopThread(){
        if (m_thread!=null){
            m_thread.stopThread();
        }
    }

    public void showInformation(){
        if(m_Bitmap==null){
            return;
        }

        //Log.d("devcpp", "showInformation -> start");

        AsyncTask<String,String,String> asyncTask = new AsyncTask<String,String,String>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                m_progressTextView.setText("wait...");
            }

            @Override
            protected String doInBackground(String... params) {
                int nWidth = m_Bitmap.getWidth();
                int nHeight = m_Bitmap.getHeight();

                int nCount = 0;
                for(int x=0; x<nWidth; x++){
                    for (int y=0; y<nHeight;y++){
                        int nColor = m_Bitmap.getPixel(x,y);
                        if(nColor==getActivity().getResources().getColor(android.R.color.white)){
                            nCount++;
                        }
                    }
                }
                return "Count " + nCount + " wite pixels";
            }

            @Override
            protected void onPostExecute(String strRes) {
                super.onPostExecute(strRes);
                m_progressTextView.setText(strRes);
            }
        };

        asyncTask.execute();

        /*m_thread = new CountThread(new Runnable() {
            @Override
            public void run() {
                //Log.d("devcpp", "Thread -> start");
                int nWidth = m_Bitmap.getWidth();
                int nHeight = m_Bitmap.getHeight();

                int nCount = 0;
                long nSize = nWidth*nHeight;
                long nDelta = nSize/20;
                long nCounter = nDelta;
                int nProcessCount = 0;
                for(int x=0; x<nWidth; x++){
                    for (int y=0; y<nHeight;y++){
                        if (m_thread.isStop()){
                            //Log.d("devcpp","m_thread.isStop()");
                            return;
                        }
                        nCounter--;
                        if(nCounter==0){
                            nProcessCount +=5;
                            Bundle bundle = new Bundle();
                            bundle.putInt(KEY_TYPE,TYPE_IN_PROGRESS);
                            bundle.putInt(KEY_PROGRESS,nProcessCount);
                            Message msg = new Message();
                            msg.setData(bundle);
                            m_handler.sendMessage(msg);
                            nCounter = nDelta;
                        }
                        int nColor = m_Bitmap.getPixel(x,y);
                        if(nColor==getActivity().getResources().getColor(android.R.color.white)){
                            nCount++;
                        }
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putInt(KEY_TYPE,TYPE_COMPLITE);
                bundle.putLong(KEY_COUNT,nCount);
                Message msg = new Message();
                msg.setData(bundle);
                m_handler.sendMessage(msg);
            }
        });
        m_thread.start();*/

        /*final CountDownLatch doneSignal = new CountDownLatch(3);

        final Thread thread2 = new CountThread(new Runnable() {
            @Override
            public void run() {
                Log.d("devcpp", "thread2 -> start");
                int nWidth = m_Bitmap.getWidth();
                int nHeight = m_Bitmap.getHeight();

                m_nRedCount = 0;
                for(int x=0; x<nWidth; x++){
                    for (int y=0; y<nHeight;y++){
                        int nColor = m_Bitmap.getPixel(x,y);
                        if(nColor==getActivity().getResources().getColor(android.R.color.holo_red_dark)){
                            m_nRedCount++;
                        }
                    }
                }
                Log.d("devcpp", "thread2 -> sleep");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d("devcpp", "thread2 -> stop");
                doneSignal.countDown();
            }
        });


        final Thread thread3 = new CountThread(new Runnable() {
            @Override
            public void run() {
                Log.d("devcpp", "thread3 -> start");
                int nWidth = m_Bitmap.getWidth();
                int nHeight = m_Bitmap.getHeight();

                m_nGreanCount = 0;
                for(int x=0; x<nWidth; x++){
                    for (int y=0; y<nHeight;y++){
                        int nColor = m_Bitmap.getPixel(x,y);
                        if(nColor==getActivity().getResources().getColor(android.R.color.darker_gray)){
                            m_nGreanCount++;
                        }
                    }
                }
                Log.d("devcpp", "thread3 -> stop");
                doneSignal.countDown();
            }
        });


        final Thread thread4 = new CountThread(new Runnable() {
            @Override
            public void run() {
                Log.d("devcpp", "thread4 -> start");
                int nWidth = m_Bitmap.getWidth();
                int nHeight = m_Bitmap.getHeight();

                m_nBlueCount = 0;
                for(int x=0; x<nWidth; x++){
                    for (int y=0; y<nHeight;y++){
                        int nColor = m_Bitmap.getPixel(x,y);
                        if(nColor==getActivity().getResources().getColor(android.R.color.holo_blue_dark)){
                            m_nBlueCount++;
                        }
                    }
                }
                Log.d("devcpp", "thread4 -> sleep");
                try {
                    Thread.sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d("devcpp", "thread4 -> stop");
                doneSignal.countDown();
            }
        });

        Thread thread5 = new CountThread(new Runnable() {
            @Override
            public void run() {
                thread2.start();
                thread3.start();
                thread4.start();
                try {
                    doneSignal.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d("devcpp", "showInfo");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(),"Red -> " + m_nRedCount + "; Blue -> " + m_nBlueCount + "; Green -> " + m_nGreanCount,Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        thread5.start();*/

        //Log.d("devcpp", "showInformation -> end");
        /*new Thread(new Runnable() {
            @Override
            public void run() {
                Random rand = new Random();
                int nSize = rand.nextInt(15) + 1;
                CountDownLatch doneSignal = new CountDownLatch(nSize);

                ExecutorService executorService = Executors.newFixedThreadPool(2);

                for(int i=0;i<nSize;i++){
                    executorService.execute(new DownloadRunnable(i,(rand.nextInt(10)*100), rand.nextInt(10), doneSignal));
                }

                try {
                    doneSignal.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Log.d("devcpp", "showInfo");
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(),"Done!",Toast.LENGTH_LONG).show();
                    }
                });
            }
        }).start();*/

    }

    public class CountThread extends Thread{

        private boolean m_bIsStop = false;

        public CountThread(Runnable r){
            super(r);
            m_bIsStop = false;
        }

        public boolean isStop(){
            return m_bIsStop;
        }

        public void stopThread(){
            Log.d("devcpp", "stopThread");
            m_bIsStop = true;
        }
    }

    public class DownloadRunnable implements Runnable{

        private int m_nPosition = 0;
        private long m_nTimeSleep = 0;
        private long m_nIntCount = 0;
        private CountDownLatch m_syncDown = null;

        public DownloadRunnable(int nPosition, long nTimeSleep, long nIntCount, CountDownLatch syncDown){
            m_nPosition = nPosition;
            m_nTimeSleep = nTimeSleep;
            m_nIntCount = nIntCount;
            m_syncDown = syncDown;
        }

        @Override
        public void run() {
            //Этот код эмулирует работу разную по времени
            Log.d("devcpp", "start -> " + m_nPosition);
            for (long i=0; i<m_nIntCount; i++){
                try {
                    Thread.sleep(m_nTimeSleep);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.d("devcpp", "end -> " + m_nPosition);
            //ОБЯЗАТЕЛЬНО ВЫЗЫВАЕМ ДЛЯ СИНХРОНИЗАЦИИ
            m_syncDown.countDown();
        }
    }
}
