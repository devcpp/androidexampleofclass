package com.kkrasylnykov.l21_fileandpermitionexample;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;

public class ViewActivity extends AppCompatActivity {

    public static final String KEY_FILE_PATH = "KEY_FILE_PATH";

    private ImageView m_imageView = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        m_imageView = (ImageView) findViewById(R.id.ImageView);
        Intent intent = getIntent();
        Bundle extra = intent.getExtras();
        String strFilePath = "";
        if(extra!=null){
            strFilePath = extra.getString(KEY_FILE_PATH, "");
        }

        if (!strFilePath.isEmpty()){
            File imageFile = new File(strFilePath);
            if(imageFile.exists()){
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(),bmOptions);
                m_imageView.setImageBitmap(bitmap);
            }
        }
    }
}
