package com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.DBWrappers;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;

public abstract class BaseDBWrappers {

    private String m_strTableName = "";
    private Context m_context = null;

    public BaseDBWrappers(Context context, String strTableName){
        m_context = context;
        m_strTableName = strTableName;
    }

    public String getTableName() {
        return m_strTableName;
    }

    public Context getContext(){
        return m_context;
    }

    public SQLiteDatabase getDbWrite(){
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db;
    }

    public SQLiteDatabase getDbRead(){
        DBHelper dbHelper = new DBHelper(getContext());
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        return db;
    }
}
