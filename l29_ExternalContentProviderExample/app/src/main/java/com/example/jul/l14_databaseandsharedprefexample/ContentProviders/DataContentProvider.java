package com.example.jul.l14_databaseandsharedprefexample.ContentProviders;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;
import com.example.jul.l14_databaseandsharedprefexample.R;

public class DataContentProvider extends ContentProvider {

    private static final String AUTHORITY = "com.kkrasylnykov.l21_fileandpermitionexample.DataContentProvider";

    public static final String USER_INFO     = "user_info";

    public static final Uri USER_INFO_URI    = Uri.parse("content://" + AUTHORITY + "/" + USER_INFO);

    private static final int URI_USER_INFO      = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, USER_INFO, URI_USER_INFO);


    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor result = null;
        switch (uriMatcher.match(uri)) {
            case URI_USER_INFO:
                DBHelper dbHelper = new DBHelper(getContext());
                SQLiteDatabase db = dbHelper.getReadableDatabase();
                result = db.query(DBHelper.TABLE_NAME_PERSONAL_INFO, projection, selection, selectionArgs, null, null, sortOrder);
                break;
        }
        return result;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d("devcppCP", "update ->");
        switch (uriMatcher.match(uri)) {
            case URI_USER_INFO:
                Log.d("devcppCP", "update ->URI_USER_INFO -> " + selection);
                Log.d("devcppCP", "update ->URI_USER_INFO -> " + values.get(DBHelper.PERSONAL_INFO_FIELD_NAME));
                DBHelper dbHelper = new DBHelper(getContext());
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.update(DBHelper.TABLE_NAME_PERSONAL_INFO,
                        values,
                        selection,
                        selectionArgs);
                break;
        }
        return 0;
    }
}
