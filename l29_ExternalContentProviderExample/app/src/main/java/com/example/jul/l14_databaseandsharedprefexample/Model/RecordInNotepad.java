package com.example.jul.l14_databaseandsharedprefexample.Model;


import android.content.ContentValues;
import android.database.Cursor;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RecordInNotepad extends BaseEntity {

    private long m_nServerId = -1;
    private String m_strName = "";
    private String m_strSName = "";
    private ArrayList<PhoneNumber> m_arrPhones = new ArrayList<>();

    public RecordInNotepad(JSONObject jsonObject){
        super(null);

        try {
            setServerId(jsonObject.getLong("id"));
            setName(jsonObject.optString("name",""));
            setSName(jsonObject.optString("sname",""));

            JSONArray jsonArrPhones = jsonObject.getJSONArray("phones");
            int nCount = jsonArrPhones.length();
            ArrayList<PhoneNumber> arrPhones = new ArrayList<>();
            for (int i=0; i<nCount;i++){
                arrPhones.add(new PhoneNumber(jsonArrPhones.getString(i)));
            }
            setPhone(arrPhones);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public RecordInNotepad(Cursor cursor){
        super(cursor);
        setId(cursor.getInt(0));
        setServerId(cursor.getLong(1));
        setName(cursor.getString(2));
        setSName(cursor.getString(3));
    }

    public RecordInNotepad(long nId, Cursor cursor){
        super(cursor);
        setId(nId);
        setServerId(cursor.getLong(0));
        setName(cursor.getString(1));
        setSName(cursor.getString(2));
    }

    public RecordInNotepad(String strName, String strSName){
        super(null);
        setName(strName);
        setSName(strSName);
    }

    public void setServerId(long m_nServerId) {
        this.m_nServerId = m_nServerId;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public void setPhone(ArrayList<PhoneNumber> arrPhones) {
        this.m_arrPhones = arrPhones;
    }

    public long getServerId() {
        return m_nServerId;
    }

    public String getName() {
        return m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public ArrayList<PhoneNumber> getPhone() {
        return m_arrPhones;
    }

    public ContentValues getContentValues(){
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_SERVER_ID,getServerId());
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_NAME,getName());
        contentValues.put(DBHelper.PERSONAL_INFO_FIELD_SNAME,getSName());
        return contentValues;
    }
}
