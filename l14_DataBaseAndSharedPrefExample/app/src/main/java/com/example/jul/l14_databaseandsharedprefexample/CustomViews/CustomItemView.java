package com.example.jul.l14_databaseandsharedprefexample.CustomViews;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jul.l14_databaseandsharedprefexample.R;

public class CustomItemView extends LinearLayout {

    private int m_nId = -1;
    private TextView m_tvName = null;
    private TextView m_tvSName = null;
    private TextView m_tvPhone = null;

    public CustomItemView(Context context) {
        super(context);
        initView(context);
    }

    public CustomItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CustomItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context){
        //Добавляем лаяут
        LayoutInflater.from(context).inflate(R.layout.custom_view_item,this, true);

        setOrientation(VERTICAL);

        //Связываем классы с UI
        m_tvName = (TextView) findViewById(R.id.tvNameItem);
        m_tvSName = (TextView) findViewById(R.id.tvSNameItem);
        m_tvPhone = (TextView) findViewById(R.id.tvPhoneItem);
    }

    //Функция установки данных
    public void setInfo(int nId, String strName, String strSName, String strPhone){
        //Устанавливаем данные
        m_nId = nId;
        m_tvName.setText(strName);
        m_tvSName.setText(strSName);
        m_tvPhone.setText(strPhone);
    }

    public int getRecId(){
        return m_nId;
    }
}
