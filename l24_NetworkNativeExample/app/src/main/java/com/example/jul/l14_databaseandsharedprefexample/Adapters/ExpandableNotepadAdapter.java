package com.example.jul.l14_databaseandsharedprefexample.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.jul.l14_databaseandsharedprefexample.Model.PhoneNumber;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.R;

import java.util.ArrayList;

public class ExpandableNotepadAdapter extends BaseExpandableListAdapter {
    ArrayList<RecordInNotepad> m_arrData = null;

    public ExpandableNotepadAdapter(ArrayList<RecordInNotepad> arrData){
        m_arrData = arrData;
    }

    @Override
    public int getGroupCount() {
        return m_arrData.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return m_arrData.get(groupPosition).getPhone().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return m_arrData.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return ((RecordInNotepad)getGroup(groupPosition)).getPhone().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return ((RecordInNotepad)getGroup(groupPosition)).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return ((PhoneNumber)getChild(groupPosition,childPosition)).getId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(android.R.layout.simple_expandable_list_item_1, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        RecordInNotepad record = (RecordInNotepad)getGroup(groupPosition);
        tv.setText(record.getName() + " " + record.getSName());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater li = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        PhoneNumber phone = (PhoneNumber)getChild(groupPosition, childPosition);
        tv.setText(phone.getPhone());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
