package com.example.jul.l14_databaseandsharedprefexample.ToolsAndConstans;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


/*Класс AppSettings явзяется синглотом, для хранения настрок (по принципу ключ-значение)
нашего приложения*/
public class AppSettings {

    //Ключи для получения настроек
    private static final String KEY_BOOLEAN_IS_FIRST_START_APP = "KEY_BOOLEAN_IS_FIRST_START_APP";

    //Указатель на себя, для реализации синглтона
    private static AppSettings m_instance = null;

    //Указатель на объект SharedPreferences, который и обеспечивает хранение настроек
    private SharedPreferences m_SharedPreferences = null;

    //Приватный конструктор, для того, что бы только сам класс мог создать экземпляр класса
    private AppSettings(Context context){
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    //Функция ждя получения доступа к экземпляру класса
    public static  AppSettings getInstance(Context context){
        if(m_instance==null){
            m_instance = new AppSettings(context);
        }
        return m_instance;
    }

    //Функции для сохранение настроек
    public void setIsFirstStartApp(boolean bIsFirstStartApp){
        //Получаем класс Editor, который обеспечивает запись данных
        SharedPreferences.Editor editor =  m_SharedPreferences.edit();
        //Вносим необходимые изменения
        editor.putBoolean(KEY_BOOLEAN_IS_FIRST_START_APP,bIsFirstStartApp);
        //Коммитим данные
        editor.commit();
    }

    //Функции для чтения настроек
    public boolean getIsFirstStartApp(){
        //Читаем данные по ключу, вторым значением передаем дефолтное значение, если данных по ключу еще нет.
        return m_SharedPreferences.getBoolean(KEY_BOOLEAN_IS_FIRST_START_APP, true);
    }
}
