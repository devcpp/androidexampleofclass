package com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.NetworkWrapper;

import android.content.Context;

public class BaseNetworkWrapper {
    private String m_strNetworkAddress = "http://xutpuk.pp.ua/";

    private Context m_context = null;

    public BaseNetworkWrapper(Context context){
        m_context = context;
    }

    public Context getContext() {
        return m_context;
    }

    public String getNetworkAddress() {
        return m_strNetworkAddress;
    }
}
