package com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.NetworkWrapper;


import android.content.Context;
import android.util.Log;

import com.example.jul.l14_databaseandsharedprefexample.Model.PhoneNumber;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class RecordNetworkWrapper extends BaseNetworkWrapper {

    public RecordNetworkWrapper(Context context) {
        super(context);
    }

    public boolean sendInsertNewRecord(RecordInNotepad item){
        boolean bResult = false;
        String strURL = getNetworkAddress() + "api/users.json";
        String strBody = "";

        JSONObject jsonBody = new JSONObject();
        try {
            jsonBody.put("name",item.getName());
            jsonBody.put("sname",item.getSName());

            JSONArray jsonArrPhones = new JSONArray();
            ArrayList<PhoneNumber> arrPhones = item.getPhone();
            for(PhoneNumber phone:arrPhones){
                jsonArrPhones.put(phone.getPhone());
            }
            jsonBody.put("phones",jsonArrPhones);

        } catch (JSONException e) {
            e.printStackTrace();
            return bResult;
        }

        strBody = jsonBody.toString();
        Log.d("devcpp", "RunnablePOST -> strRequest -> " + strBody);

        try {
            URL url = new URL(strURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
            connection.getOutputStream().write(postData);

            connection.connect();

            int response = connection.getResponseCode();
            if(response==200){

                InputStream is = connection.getInputStream();
                String strResponse = "";
                if (is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }

                Log.d("devcpp", "strResponse -> " + strResponse);
                JSONObject jsonResponse = new JSONObject(strResponse);
                long nServerId = jsonResponse.getLong("id");

                item.setServerId(nServerId);
                bResult = true;
            }

            return bResult;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return  bResult;
    }

    public ArrayList<RecordInNotepad> getAllRecordsFromServer(){
        ArrayList<RecordInNotepad> arrReturn = new ArrayList<>();
        String strURL = getNetworkAddress() + "api/users.json";
        URL url = null;
        try {
            url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");
            connection.setDoInput(true);
            connection.connect();

            int response = connection.getResponseCode();
            if(response==200){
                InputStream inputStream = connection.getInputStream();
                String strResponse = "";
                if (inputStream!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }

                JSONArray jsonArrResponse = new JSONArray(strResponse);
                int nCount = jsonArrResponse.length();
                for (int i=0; i<nCount;i++){
                    //Обязательно указываем тип получаемого значения, т.к. членом массива может быть массив.
                    JSONObject jsonObject = jsonArrResponse.getJSONObject(i);
                    arrReturn.add(new RecordInNotepad(jsonObject));
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrReturn;
    }
}
