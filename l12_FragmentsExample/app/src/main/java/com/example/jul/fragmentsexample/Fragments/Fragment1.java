package com.example.jul.fragmentsexample.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.jul.fragmentsexample.Activites.MainActivity;
import com.example.jul.fragmentsexample.R;

public class Fragment1 extends Fragment implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_1,null);
        Button btn = (Button) view.findViewById(R.id.F1B1);
        btn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        /*Вызываем фугкцию getActivity() и приводим тип возврата к классу MainActivity и вызываем
         setNewTextInFragment2 объявленную в MainActivity*/
        ((MainActivity)getActivity()).setNewTextInFragment2("New Text");
    }
}
