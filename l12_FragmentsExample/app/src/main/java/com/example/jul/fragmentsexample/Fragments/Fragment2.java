package com.example.jul.fragmentsexample.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.jul.fragmentsexample.R;

public class Fragment2 extends Fragment {

    TextView m_tv = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_2,null);
        m_tv = (TextView) view.findViewById(R.id.F2TV2);
        return view;
    }

    /*Объявляем функцию для установки текста в втором фрагменте*/
    public void setNewText(String strText){
        m_tv.setText(strText);
    }
}
