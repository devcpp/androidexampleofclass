package com.example.jul.fragmentsexample.Activites;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.jul.fragmentsexample.Fragments.Fragment1;
import com.example.jul.fragmentsexample.Fragments.Fragment2;
import com.example.jul.fragmentsexample.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Fragment1 m_fragment1 = null;
    Fragment2 m_fragment2 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button1 = (Button) findViewById(R.id.button1);
        Button button2 = (Button) findViewById(R.id.button2);

        m_fragment1 = new Fragment1();
        m_fragment2 = new Fragment2();

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //Создаем новую транзакцию
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        if(v.getId()==R.id.button1){
            //Добавляем первый фрагмент в транзакцию
            fragmentTransaction.add(R.id.FrameLayout1, m_fragment1);
        } else if(v.getId()==R.id.button2) {
            //Добавляем второй фрагмент в транзакцию
            fragmentTransaction.add(R.id.FrameLayout2, m_fragment2);
        }

        //Комитим текущую транзакцию
        fragmentTransaction.commit();
    }

    //Объявляем функцию которая передает данные в второй фрагмент
    public void setNewTextInFragment2(String strText){
        m_fragment2.setNewText(strText);
    }
}
