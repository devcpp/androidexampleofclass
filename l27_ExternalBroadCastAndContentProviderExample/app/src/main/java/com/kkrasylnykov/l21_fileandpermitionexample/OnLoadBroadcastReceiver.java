package com.kkrasylnykov.l21_fileandpermitionexample;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class OnLoadBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("devcpp", "OnLoadBroadcastReceiver -> onReceive");
        Intent intentService = new Intent(context, OnLoadService.class);
        context.startService(intentService);

    }
}
