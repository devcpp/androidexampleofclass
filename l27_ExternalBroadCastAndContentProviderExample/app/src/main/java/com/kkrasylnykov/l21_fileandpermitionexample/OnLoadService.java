package com.kkrasylnykov.l21_fileandpermitionexample;


import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class OnLoadService extends Service {

    Thread m_Thread = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("devcpp", "OnLoadService -> onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("devcpp", "OnLoadService -> onStartCommand");
        //Останавливает работу сервиса
//        stopSelf();
        if (m_Thread==null){
            m_Thread= new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true){
                        try {
                            int nCount = ExampleContentProviderWrapper.getCountExec(OnLoadService.this);
                            Log.d("devcpp","OnLoadService -> nCount -> " + nCount);
                            Thread.sleep(7000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            m_Thread.start();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
