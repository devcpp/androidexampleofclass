package com.kkrasylnykov.l21_fileandpermitionexample;


import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.preference.PreferenceManager;

public class ExampleContentProviderWrapper {

    public static void setCountExec(Context context, int nCount){
        Uri uri = ExampleContentProvider.COUNT_EXEC_URI;
        ContentValues contentValues = new ContentValues();
        contentValues.put(ExampleContentProvider.KEY_DATA, nCount);
        context.getContentResolver().insert(uri, contentValues);

    }

    public static int getCountExec(Context context){
        int nResult = -1;
        Cursor cursor = null;
        Uri uri = ExampleContentProvider.COUNT_EXEC_URI;
        cursor = context.getContentResolver().query(uri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst()){
            nResult = cursor.getInt(0);
        }
        if(cursor != null){
            cursor.close();
        }
        return nResult;
    }
}
