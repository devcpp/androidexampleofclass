package com.kkrasylnykov.l21_fileandpermitionexample;


public class FileOnServer {
    private int m_nId = -1;
    private String m_strLocalName = "";
    private String m_strServerUrl = "";

    public FileOnServer(int nId,String strLocalName, String strServerUrl) {
        m_nId = nId;
        this.m_strLocalName = strLocalName;
        this.m_strServerUrl = strServerUrl;
    }

    public int getId() {
        return m_nId;
    }

    public void setId(int nId) {
        this.m_nId = nId;
    }

    public String getLocalName() {
        return m_strLocalName;
    }

    public void setM_strLocalName(String strLocalName) {
        this.m_strLocalName = strLocalName;
    }

    public String getServerUrl() {
        return m_strServerUrl;
    }

    public void setServerUrl(String strServerUrl) {
        this.m_strServerUrl = strServerUrl;
    }
}
