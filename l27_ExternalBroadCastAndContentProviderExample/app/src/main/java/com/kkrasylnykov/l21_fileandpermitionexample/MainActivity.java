package com.kkrasylnykov.l21_fileandpermitionexample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_PERMISSION_ON_ATTACH_FILE = 1;

    ListView m_FilesListView = null;

    ArrayList<FileOnServer> m_arrData = null;
    AdapterListView m_adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intentService = new Intent(this, OnLoadService.class);
        startService(intentService);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true){
                    try {
                        int nCount = ExampleContentProviderWrapper.getCountExec(MainActivity.this) +1;
                        ExampleContentProviderWrapper.setCountExec(MainActivity.this,nCount);
                        Log.d("devcpp","MainActivity -> nCount -> " + nCount);
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_ATTACH_FILE);
        }else{
            onCreate();
        }
    }

    public static ArrayList<FileOnServer> getListOfFileOnServer(){
        ArrayList<FileOnServer> arrResult = new ArrayList<>();
        arrResult.add(new FileOnServer(1,"file0.png","https://pp.vk.me/c630216/v630216587/38e0d/xX2qGqsZrGE.jpg"));
        arrResult.add(new FileOnServer(2,"file1.png","https://pp.vk.me/c630216/v630216587/38e17/yaJqhxL1UD8.jpg"));
        arrResult.add(new FileOnServer(3,"file2.png","https://pp.vk.me/c630216/v630216587/38e3f/0LdLK_GtvZo.jpg"));
        arrResult.add(new FileOnServer(4,"file3.png","https://pp.vk.me/c630216/v630216587/38e35/N3_Q1J621zU.jpg"));
        arrResult.add(new FileOnServer(5,"file4.png","https://pp.vk.me/c630216/v630216587/38e49/vGE22vZUuMk.jpg"));
        return arrResult;
    }

    private void onCreate(){
        m_arrData =getListOfFileOnServer();

        m_FilesListView = (ListView) findViewById(R.id.FilesListView);
        m_adapter = new AdapterListView(m_arrData);
        m_FilesListView.setAdapter(m_adapter);
        m_FilesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentViewActivity = new Intent(MainActivity.this, ViewPagerActivity.class);
                intentViewActivity.putExtra(ViewPagerActivity.KEY_POSITION, position);
                startActivity(intentViewActivity);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==REQUEST_PERMISSION_ON_ATTACH_FILE){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MainActivity.this, "Приложение не может работать без разрешения", Toast.LENGTH_LONG).show();
                finish();
            }else {
                onCreate();
            }
        }
    }
}
