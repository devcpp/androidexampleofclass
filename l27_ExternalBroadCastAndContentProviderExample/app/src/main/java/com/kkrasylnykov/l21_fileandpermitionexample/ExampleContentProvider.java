package com.kkrasylnykov.l21_fileandpermitionexample;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

public class ExampleContentProvider extends ContentProvider {

    public static final String KEY_DATA = "KEY_DATA";

    private static final String AUTHORITY = "com.kkrasylnykov.l21_fileandpermitionexample.ExampleContentProvider";

    public static final String COUNT_EXEC     = "count_exec";

    public static final Uri COUNT_EXEC_URI    = Uri.parse("content://" + AUTHORITY + "/" + COUNT_EXEC);

    private static final int URI_COUNT_EXEC      = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, COUNT_EXEC, URI_COUNT_EXEC);


    }

    private SharedPreferences m_SharedPreferences = null;

    @Override
    public boolean onCreate() {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Object returnData = null;
        switch (uriMatcher.match(uri)) {
            case URI_COUNT_EXEC:
                returnData = new Integer(m_SharedPreferences.getInt(COUNT_EXEC,0));
                break;
        }
        String[] arrColumnNames = {"Data"};
        Object[] arrValue = {returnData};
        MatrixCursor matrixCursor = new MatrixCursor(arrColumnNames);
        matrixCursor.addRow(arrValue);
        return matrixCursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        switch (uriMatcher.match(uri)) {
            case URI_COUNT_EXEC:
                int nCountExec = values.getAsInteger(KEY_DATA);
                SharedPreferences.Editor editor =  m_SharedPreferences.edit();
                editor.putInt(COUNT_EXEC,nCountExec);
                editor.commit();
                break;
        }
        return uri;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }
}
