package com.kkrasylnykov.l21_fileandpermitionexample;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_PERMISSION_ON_ATTACH_FILE = 1;

    ListView m_FilesListView = null;

    ArrayList<File> m_arrData = null;
    AdapterListView m_adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_ON_ATTACH_FILE);
        }else{
            onCreate();
        }
    }

    private void onCreate(){
        m_arrData = getJpgFiles(Environment.getExternalStorageDirectory());

        m_FilesListView = (ListView) findViewById(R.id.FilesListView);
        m_adapter = new AdapterListView(m_arrData);
        m_FilesListView.setAdapter(m_adapter);
        m_FilesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentViewActivity = new Intent(MainActivity.this, ViewActivity.class);
                intentViewActivity.putExtra(ViewActivity.KEY_FILE_PATH, ((File)m_adapter.getItem(position)).getAbsolutePath());
                startActivity(intentViewActivity);
            }
        });
    }

    private ArrayList<File> getJpgFiles(File file){
        ArrayList<File> arrReturn = new ArrayList<>();
        if (file.isDirectory()){
            File[] arrFiles = file.listFiles();
            for (File curFile:arrFiles){
                arrReturn.addAll(getJpgFiles(curFile));
            }
        } else {
            if(file.getName().contains(".jpg")){
                arrReturn.add(file);
            }
        }
        return arrReturn;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==REQUEST_PERMISSION_ON_ATTACH_FILE){
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED){
                Toast.makeText(MainActivity.this, "Приложение не может работать без разрешения", Toast.LENGTH_LONG).show();
                finish();
            }else {
                onCreate();
            }
        }
    }
}
