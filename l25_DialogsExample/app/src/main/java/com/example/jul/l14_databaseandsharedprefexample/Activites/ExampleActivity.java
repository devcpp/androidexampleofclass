package com.example.jul.l14_databaseandsharedprefexample.Activites;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.jul.l14_databaseandsharedprefexample.R;

import java.util.Calendar;

public class ExampleActivity extends AppCompatActivity implements View.OnClickListener {

    TextView m_numberTextView = null;
    TextView m_dataTextView = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        m_numberTextView = (TextView)  findViewById(R.id.numberExapleTextView);
        m_numberTextView.setOnClickListener(this);

        m_dataTextView = (TextView)  findViewById(R.id.dataExapleTextView);
        m_dataTextView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.numberExapleTextView:

                //Такого диалога в системе Android нет - реализуется как кастомный
                /*Dialog numberPickerDialog = new Dialog(this);
                NumberPicker numberPicker = new NumberPicker(this);
                numberPicker.setMaxValue(240);
                numberPicker.setMinValue(120);
                numberPicker.setValue(160);
                numberPickerDialog.setContentView(numberPicker);
                numberPickerDialog.show();*/
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        m_numberTextView.setText(hourOfDay + ":" + minute);
                    }
                };

                Calendar calendar = Calendar.getInstance();

                TimePickerDialog timePickerDialog =  new TimePickerDialog(this, onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
                timePickerDialog.show();

                break;
            case R.id.dataExapleTextView:
                DatePickerDialog.OnDateSetListener onDateChangedListener = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Log.d("devcppDate"," -> " + ((monthOfYear<9)?"0":""));
                        m_dataTextView.setText(((dayOfMonth<10)?"0":"") + dayOfMonth+"/" + ((monthOfYear<9)?"0":"") +(monthOfYear+1) + "/" +year);
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateChangedListener, 2016, 7, 7);
                datePickerDialog.show();
                break;
        }
    }
}
