package com.example.jul.l14_databaseandsharedprefexample.Activites;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.example.jul.l14_databaseandsharedprefexample.Adapters.ExpandableNotepadAdapter;
import com.example.jul.l14_databaseandsharedprefexample.Adapters.NotepadAdapter;
import com.example.jul.l14_databaseandsharedprefexample.Dialogs.WaitingDialog;
import com.example.jul.l14_databaseandsharedprefexample.Model.DataEngine.RecordEngine;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;
import com.example.jul.l14_databaseandsharedprefexample.R;
import com.example.jul.l14_databaseandsharedprefexample.ToolsAndConstans.AppSettings;

import java.util.ArrayList;

public class MainListActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    ExpandableListView m_listView = null;

    ArrayList<RecordInNotepad> m_arrRecords = new ArrayList<>();
    ExpandableNotepadAdapter m_adapter = null;

    String m_strSerch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);

        AppSettings settings = AppSettings.getInstance(this);

        View btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        View btnRemoveAll = findViewById(R.id.btnRemoveAll);
        btnRemoveAll.setOnClickListener(this);

        View btnExample = findViewById(R.id.btnExample);
        btnExample.setOnClickListener(this);

        EditText editText = (EditText) findViewById(R.id.searchEditText);
        editText.addTextChangedListener(new TextWatcher() {
            String strOld = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                m_strSerch = s.toString();
                viewInfo();
            }
        });


        m_listView = (ExpandableListView) findViewById(R.id.list_view);
        m_adapter = new ExpandableNotepadAdapter(m_arrRecords);
        m_listView.setAdapter(m_adapter);
        m_listView.setOnItemClickListener(this);

        if (settings.getIsFirstStartApp()){
            settings.setIsFirstStartApp(false);

            Toast.makeText(this, "First Start", Toast.LENGTH_LONG).show();

            final  WaitingDialog waitingDialog = new WaitingDialog(this);
            waitingDialog.setMessage("Подождите пожайлуста. Идет загрузка. ");
            waitingDialog.show();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    RecordEngine engine = new RecordEngine(MainListActivity.this);
                    engine.getAllRecordsFromServer();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainListActivity.this, "Show info!", Toast.LENGTH_LONG).show();
                            viewInfo();
                            waitingDialog.dismiss();
                        }
                    });
                }
            }).start();
        } else {
            Toast.makeText(this, "No First Start", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnAdd:
                Intent intentCompouseActivity = new Intent(this,CompouseActivity.class);
                startActivity(intentCompouseActivity);
                break;
            case R.id.btnExample:
                Intent intentExampleActivity = new Intent(this,ExampleActivity.class);
                startActivity(intentExampleActivity);
                break;
            case R.id.btnRemoveAll:
                Log.d("devcppDebug","btnRemoveAll");
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Warning!!!!!");
                builder.setCancelable(true);
                builder.setMessage("Вы хочете удалить все записи?");
                DialogInterface.OnClickListener positiveOnClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        RecordEngine engine = new RecordEngine(MainListActivity.this);
                        engine.removeAll();
                        viewInfo();
                    }
                };
                builder.setPositiveButton("Да!",positiveOnClickListener);

                DialogInterface.OnClickListener negativeOnClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                builder.setNegativeButton("Нет!",negativeOnClickListener);

                DialogInterface.OnClickListener neutralOnClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                };
                builder.setNeutralButton("Чо?",neutralOnClickListener);
                builder.show();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        viewInfo();
    }

    private void viewInfo(){
        m_arrRecords.clear();
        RecordEngine engine = new RecordEngine(this);
        if(m_strSerch.length()>0){
            Log.d("devcpp", "m_strSerch -> " + m_strSerch);
            m_arrRecords.addAll(engine.getListBySearchString(m_strSerch));
        } else {
            m_arrRecords.addAll(engine.getAll());
        }

        m_adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.d("devcpp", "position -> " + position);
        Log.d("devcpp", "id -> " + id);
        Intent updateIntent = new Intent(this, CompouseActivity.class);
        updateIntent.putExtra(CompouseActivity.EXTRA_KEY_ID_REVORD, id);
        startActivity(updateIntent);
    }
}
