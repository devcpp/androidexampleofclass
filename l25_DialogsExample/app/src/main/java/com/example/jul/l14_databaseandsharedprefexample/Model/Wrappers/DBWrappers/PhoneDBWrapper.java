package com.example.jul.l14_databaseandsharedprefexample.Model.Wrappers.DBWrappers;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.jul.l14_databaseandsharedprefexample.DB.DBHelper;
import com.example.jul.l14_databaseandsharedprefexample.Model.PhoneNumber;
import com.example.jul.l14_databaseandsharedprefexample.Model.RecordInNotepad;

import java.util.ArrayList;

public class PhoneDBWrapper extends BaseDBWrappers {
    public PhoneDBWrapper(Context context) {
        super(context, DBHelper.TABLE_NAME_PHONES);
    }

    public ArrayList<PhoneNumber> getPhonesByInfoId(long nIdInfo){
        ArrayList<PhoneNumber> arrResult = new ArrayList<>();
        SQLiteDatabase db = getDbRead();
        String strRec = DBHelper.PHONES_FIELD_ID_INFO + " = ?";
        String[] arrSerch = new String[]{Long.toString(nIdInfo)};
        Cursor cursor = db.query(getTableName(), null, strRec, arrSerch, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            do {
                arrResult.add(new PhoneNumber(cursor));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return arrResult;
    }

    public long insertItem(PhoneNumber item){
        SQLiteDatabase db = getDbWrite();
        return db.insert(getTableName(), null, item.getContentValues());
    }

    public void updateItem(PhoneNumber item){
        SQLiteDatabase db = getDbWrite();
        db.update(getTableName(), item.getContentValues(),
                DBHelper.PHONES_FIELD_ID + "=?",
                new String[]{Long.toString(item.getId())});
    }

    public void removeItem(PhoneNumber item){
        SQLiteDatabase db = getDbWrite();

        String strRec = DBHelper.PHONES_FIELD_ID + " == ?";

        String[] arrSerch = new String[]{Long.toString(item.getId())};

        db.delete(getTableName(),
                strRec,
                arrSerch);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getDbWrite();

        db.delete(getTableName(),
                null,
                null);
        db.close();
    }
}
