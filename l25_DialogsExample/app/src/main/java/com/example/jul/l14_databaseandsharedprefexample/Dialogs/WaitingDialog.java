package com.example.jul.l14_databaseandsharedprefexample.Dialogs;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.jul.l14_databaseandsharedprefexample.R;

import org.w3c.dom.Text;

public class WaitingDialog extends Dialog {

    private String m_strText = "";

    public WaitingDialog(Context context) {
        super(context);
    }
    public WaitingDialog(Context context, int themeResId) {
        super(context, themeResId);
    }
    protected WaitingDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(false);
        setContentView(R.layout.dialog_waiting);
        ProgressBar progressBar = (ProgressBar) findViewById(R.id.waitingProgressBar);
        progressBar.setMax(Integer.MAX_VALUE);

        TextView textView = (TextView) findViewById(R.id.waitingTextView);
        textView.setText(m_strText);
    }

    public void setMessage(String strText) {
        m_strText = strText;
    }
}
